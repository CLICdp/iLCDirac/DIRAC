.. _framework_monitoring:

================================
The Framework/monitoring service
================================

The framework system for the monitoring of services and agents has been removed and will no longer be used.

It has been replaced by an OpenSearch-based monitoring system. You can read about it in :ref:`Monitoring <monitoring_system>`
